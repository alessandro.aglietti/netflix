'use strict';
 
const LightningClient = require('lightning-client');
const cookieParser = require('cookie-parser');
const express = require('express');
const uuid = require('uuid/v4');
const fs = require('fs');

const app = express();

const client = new LightningClient('/tmp/ln1', true);

const chunksMap = {};
const paidInvoices = {};

const mSatPerByte = 0.01;

app.use(cookieParser());

const sessionMiddleware = (req, res, next) => {
	if (!req.cookies['_mb_session']) {
		req.cookies['_mb_session'] = uuid();
		res.cookie('_mb_session', req.cookies['_mb_session'], { maxAge: 900000, httpOnly: true });
	}

	req.session_id = req.cookies['_mb_session'];
	next();
}

app.use(express.static('public'));

function genRangeStr(start, end) {
	return `${start}-${end}`;
}

function validateRange(start, end) {
	try {
		let _start = parseInt(start);
		let _end = parseInt(end);

		if (_start >= _end) {
			return false;
		}
	} catch (e) {
		return false;
	}

	return true;
}

app.head('/chunk/:videoFile', sessionMiddleware, (req, res) => {
	// FIXME: yeah, it's not safe, i know
	const stats = fs.statSync(req.params.videoFile);
	const fileSizeInBytes = stats.size;

	res.set('content-length', fileSizeInBytes);
	res.set('content-type', 'video/mp4; charset=utf-8');
	res.end();
});

app.get('/chunk_available', sessionMiddleware, (req, res) => {
	if (!validateRange(req.query.start, req.query.end)) {
		return res.status(400).send({error: 'BAD_REQUEST'});
	}

	const rangeStr = genRangeStr(req.query.start, req.query.end);

	if (!chunksMap[req.session_id]) {
		chunksMap[req.session_id] = {};
	}

	if (!chunksMap[req.session_id][rangeStr] || !paidInvoices[chunksMap[req.session_id][rangeStr]]) {
		return res.status(403).send({error: true});
	}

	res.send({error: null});
});

app.get('/chunk/:videoFile', sessionMiddleware, (req, res) => {
	const stats = fs.statSync(req.params.videoFile);
	const fileSizeInBytes = stats.size;

	let range = '';
	if (!req.headers['range']) {
		range = `0-${fileSizeInBytes}`;
	} else {
		range = req.headers['range'].replace('bytes=', '');
	}

	const start = parseInt(range.split('-')[0]);
	const end = parseInt(range.split('-')[1]);

	if (!chunksMap[req.session_id]) {
		chunksMap[req.session_id] = {};
	}

	const rangeStr = genRangeStr(start, end);
	if (!chunksMap[req.session_id][rangeStr] || !paidInvoices[chunksMap[req.session_id][rangeStr]]) {
		return res.status(403).end();
	}

	res.set('Accept-Ranges', 'bytes');
	res.set('Content-Type', 'video/mp4; charset=utf-8');
	res.set('Content-Range', `bytes ${start}-${end + 1}/${fileSizeInBytes}`);

	// FIXME: yeah, it's not safe, i know
	// async I/O
	fs.open(req.params.videoFile, 'r', function (err, file) {
		if (err) {
			return res.status(500).send({error: err});
		}

		const buffer = new Buffer(end - start + 1);
		fs.read(file, buffer, 0, end - start + 1, start, function (err) {
			if (err) {
				return res.status(500).send({error: err});
			}

			res.send(buffer);
		});
	});
});

app.get('/invoice', sessionMiddleware, (req, res) => {
	if (!validateRange(req.query.start, req.query.end)) {
		return res.status(400).send({error: 'BAD_REQUEST'});
	}

	const bytes = (parseInt(req.query.end) - parseInt(req.query.start));
	const amount = Math.ceil(bytes * mSatPerByte);
	const rangeStr = genRangeStr(req.query.start, req.query.end);

	if (!chunksMap[req.session_id]) {
		chunksMap[req.session_id] = {};
	}

	client.listinvoices(`${req.session_id}-${rangeStr}`)
		.then(invoices => {
			if (!invoices.invoices[0]) {
				return client.invoice(amount, `${req.session_id}-${rangeStr}`, `Purchase of ${bytes} bytes of video for a total of ${amount} mSAT`);
			}

			return invoices.invoices[0];
		})
		.then(invoice => {
			chunksMap[req.session_id][rangeStr] = invoice.payment_hash;
			res.send({bolt11: invoice.bolt11});
		})
		.catch(e => {
			res.status(500).send(e);
		});
});

app.listen(3000, () => console.log('Netflix server listening on port 3000!'));

const invoicesLoop = async function(handler) {
	let lastIndex = 0;

	while (true) {
		const invoice = await client.waitanyinvoice(lastIndex)
			.then(res => {
				lastIndex = res.pay_index;
				return res;
			})
			.then(handler);
	}
}

invoicesLoop(async function (invoice) {
	if (invoice.status === 'paid') {
		paidInvoices[invoice.payment_hash] = true;
	}
});